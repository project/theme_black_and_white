/**
 * @file
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.black_and_white = {
    attach(context, settings) {
      $(document).ready(() => {
        Drupal.behaviors.black_and_white.trigBWEvent(context);
      });
    },
    trigBWEvent(context) {
      $('.black-and-white-button', context).on('click', () => {
        $.ajax({
          url: '/theme_black_and_white',
          cache: false
        }).done(data => {
          if (data === 'black-and-white') {
            $('body')
              .removeClass('colored')
              .addClass('black-and-white');
          }
          else {
            $('body')
              .addClass('colored')
              .removeClass('black-and-white');
          }
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
