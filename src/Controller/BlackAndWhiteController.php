<?php

namespace Drupal\theme_black_and_white\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\PrivateTempStoreFactory;

/**
 * Black and White controller for the session storing jobs.
 */
class BlackAndWhiteController extends ControllerBase {
  protected $tempStore;

  /**
   * Pass the dependency to the object constructor.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory) {
    $this->tempStore = $temp_store_factory->get('theme_black_and_white');
  }

  /**
   * Declare dependency to be passed to constructor.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore')
    );
  }

  /**
   * Routing endpoint for the AJAX call.
   */
  public function setBlackAndWhite() {
    $response = "black-and-white";
    if ($this->tempStore->get('bw_status') == NULL) {
      $this->tempStore->set('bw_status', "black-and-white");
    }
    else {
      if (($this->tempStore->get('bw_status') == "colored")) {
        $response = "black-and-white";
        $this->tempStore->set('bw_status', "black-and-white");
      }
      else {
        $response = "colored";
        $this->tempStore->set('bw_status', "colored");
      }
    }

    return new JsonResponse($response);
  }

}
