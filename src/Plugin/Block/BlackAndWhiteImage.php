<?php

namespace Drupal\theme_black_and_white\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'BlackAndWhiteImage' block.
 *
 * @Block(
 *  id = "black_and_white_image",
 *  admin_label = @Translation("Black and white image"),
 * )
 */
class BlackAndWhiteImage extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $module_path = drupal_get_path('module', 'theme_black_and_white');
    $build['#theme'] = 'theme_black_and_white';
    $build['#module_path'] = $module_path;
    $build['#attached'] = [
      'library' => [
        'theme_black_and_white/theme_black_and_white-library',
      ],
    ];
    $build['#attributes']['class'][] = 'black-and-white-button';
    return $build;
  }

}
