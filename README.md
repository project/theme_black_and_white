INTRODUCTION
------------

This module provides a block to change the theme to it's grayscale version
by adding a simple css to the body tag.


FEATURES
--------

* Adds a css class to the body tag on the block image click.
* Remembers the user choice by storing it's value in the user session.
* Provides a twig template to make it easy to change the styling.


INSTALLATION
------------

* Simply enable the module.


SETUP
-----

* Add the 'Black and white image' block to your choice of region from the
'Block Layout' interface.


MAINTAINERS
-----------

Current maintainers:
 * Bekir Dağ (bekirdag) - https://www.drupal.org/u/bekirdag
